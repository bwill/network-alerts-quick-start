#!/bin/bash

cd "$(dirname "$0")" || exit 1

stderr() {
  echo "$1" > /dev/stderr
}

GITLAB_HOST="${GITLAB_HOST:-$CI_SERVER_HOST}"
GITLAB_HOST=${GITLAB_HOST:-gitlab.com}
PROTO="${CI_SERVER_PROTOCOL:-https}"
PORT="${CI_SERVER_PORT:-443}"
BASE="${PROTO}://${GITLAB_HOST}"
if [ "$PORT" != 443 ] && [ "$PORT" != 80 ]; then
  # Should probably also check proto... but who runs HTTPS on port 80 or vice-versa?
  BASE="${BASE}:${PORT}"
fi
REST_URL="${BASE}/api/v4"
GRAPHQL_URL="${BASE}/api/graphql"
GITLAB_TOKEN="$CI_JOB_TOKEN"
PROJECT_ID="$CI_PROJECT_ID"

while [[ $# -gt 0 ]]; do
  key="$1"
  case "$key" in
    -t|--gitlab-token)
      GITLAB_TOKEN="$2"
      shift
      shift
      ;;
    -p|--project-id)
      PROJECT_ID="$2"
      shift
      shift
      ;;
    *)
      stderr "Unknown argument: $2"
      exit 1
      ;;
  esac
done

ARGS_VALID=1

if [ -z "$GITLAB_TOKEN" ]; then
  stderr "Must provide gitlab token"
  ARGS_VALID=0
fi

if [ -z "$PROJECT_ID" ]; then
  stderr "Must provide project id"
  ARGS_VALID=0
fi

[ "$ARGS_VALID" = 1 ] || exit 1

set -e

stderr "Environment name: $CI_ENVIRONMENT_NAME"
stderr "Kubeconfig is: ${KUBECONFIG:-~/.kube/config}"
stderr "Current context is: $(kubectl config current-context)"

agent-cli() {
  if [ -n "$CI" ]; then
    /bin/agent-cli "$@"
  else
    docker run --pull=always --rm registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:stable "$@"
  fi
}

body() {
  jq -c --null-input --arg query "$1" '.query=$query'
}

request() {
  curl -sSL \
    --header "Authorization: Bearer ${GITLAB_TOKEN}"\
    --header "Content-Type: application/json"\
    "$@"
}

PROJECT_PATH="$(request "${REST_URL}/projects/${PROJECT_ID}" | jq -r .path_with_namespace)"

install_cilium() {
  stderr 'Creatining namespace for cilium'
  kubectl get namespaces | grep cilium || kubectl create namespace cilium
  stderr 'Installing cilium'
  helmfile --file helmfile.yaml apply
  stderr 'Waiting for cilium pods to become ready'
  kubectl wait -n cilium --for=condition=Ready pod -l k8s-app=cilium
  stderr 'Restarting all pods not managed by cilium'
  kubectl get pods --all-namespaces -o custom-columns=NAMESPACE:.metadata.namespace,NAME:.metadata.name,HOSTNETWORK:.spec.hostNetwork --no-headers=true | \
    grep '<none>' | awk '{print "-n "$1" "$2}' | xargs -L 1 -r kubectl delete pod --ignore-not-found
}

setup_agent_graphql() {
  stderr 'Creating new agent in GitLab'
  agent_name="network-alerts-$(date +%s)"

  create_agent="mutation createAgent {
    # agent-name should be the same as specified in the config.yaml
    createClusterAgent(input: { projectPath: \"$PROJECT_PATH\", name: \"$agent_name\" }) {
      clusterAgent {
        id
        name
      }
      errors
    }
  }"

  resp="$(request "$GRAPHQL_URL" -d "$(body "$create_agent")")" || exit 1
  id="$(echo "$resp" | jq -r .data.createClusterAgent.clusterAgent.id)"
  stderr "Agent ID: $id"
  token_name="${agent_name}-token"

  stderr 'Creating new agent token'
  create_agent_token="mutation createToken {
  clusterAgentTokenCreate(
    input: {
      clusterAgentId: \"$id\"
      name: \"$token_name\"
    }
  ) {
    secret # This is the value you need to use on the next step
    token {
      createdAt
      id
    }
    errors
  }
}"

  resp="$(request "$GRAPHQL_URL" -d "$(body "$create_agent_token")")" || exit 1
  token_id="$(echo "$resp" | jq -r .data.clusterAgentTokenCreate.id)"
  stderr "Token ID: $token_id"
  echo "$resp" | jq -r .data.clusterAgentTokenCreate.secret
}

install_gitlab_agent() {
  AGENT_TOKEN="$1"
  stderr 'Creating namespace for gitlab agent'
  kubectl get namespaces | grep gitlab-kubernetes-agent || kubectl create namespace gitlab-kubernetes-agent
  stderr 'Installing GitLab Agent'
  agent-cli generate \
    --agent-token="$AGENT_TOKEN" \
    --kas-address=wss://kas."$GITLAB_HOST" \
    --agent-version stable \
    --namespace gitlab-kubernetes-agent | kubectl apply -f -
}

demo() {
  sed "s/{{project_id}}/${PROJECT_ID}/g" < demo.yaml | kubectl apply -f -
  kubectl wait --for=condition=Ready pods -l network-alerts-quick-start=demo
  ip="$(kubectl get pods -l app=nginx -o json | jq -r '.items[0].status.podIP')"
  kubectl exec -ti dnsutils -- curl -v "https://$ip"
  stderr "An alert should be present at ${BASE}/${PROJECT_PATH}/-/threat_monitoring"
}

install_cilium
secret="$(setup_agent_graphql)"
install_gitlab_agent "$secret"
demo
