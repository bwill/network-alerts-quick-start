FROM registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:stable as cli

# kubectl and helm only available on edge
FROM alpine:edge

COPY --from=cli /app/cmd/cli/cli_linux /bin/agent-cli

RUN apk add --update \
  bash \
  curl \
  jq \
  git

RUN curl -sLO 'https://dl.k8s.io/release/v1.21.3/bin/linux/amd64/kubectl' && \
  chmod +x kubectl && \
  mv kubectl /bin/kubectl && \
  kubectl version --client

RUN curl -sLO 'https://get.helm.sh/helm-v3.6.3-linux-amd64.tar.gz' && \
  tar -zxvf helm-v3.6.3-linux-amd64.tar.gz && \
  mv linux-amd64/helm /bin/helm && \
  rm -rf helm-v3.6.3-linux-amd64.tar.gz linux-amd64/ && \
  helm version

RUN helm plugin install https://github.com/databus23/helm-diff && \
  helm plugin list

RUN curl -sL -o helmfile 'https://github.com/roboll/helmfile/releases/download/v0.140.0/helmfile_linux_amd64' && \
  chmod +x helmfile && \
  mv helmfile /bin/helmfile && \
  helmfile version
